## GLUT TEXT DRAWING

This repo is an extension for a student project. All the non-native function and class used here are considered implemented. Nevertheless, one may find the process of drawing some text described here useful as is.

### Concept
For each letter, we create two triangles on which we apply a font texture. That's it :) 

![Font texture](./font.jpg)          |
------------------------------------------ |
*fig1. The texture font available in `./data`. Note that the texture start from top-left corner.* |

![Font texture](./example_font.jpg)  |
------------------------------------------ |
*fig2. Font drawing example.* |

### Implementation

#### Text structure
First, let's create a **structure** describing a `text`.

```c++
// Text drawing Structure
struct text{
  GLuint vao  = 0;             // Vertex array id
  GLuint vbo  = 0;             // Vertex buffer id
  GLuint vboi = 0;             // Index buffer id
  GLuint texture_id = 0;       // Well, texture id...
  std::string value;           // Value of the text to display
  transformation transform;    // Rotation & translation
  text():transform(){}
};
text text_to_draw;
```

*Notes:*
 - *We will use text_to_draw as a global variable to draw one single text*
 - *You can set the text directly, as `text_to_draw.value = "some text"`*
 - *You can set the transformation directly, as `text_to_draw.rotation = matrice_rotation( -M_PI/2, 1.0f,0.0f,0.0f);`*

#### Init function
Now, we write the function that creates the triangles on which we will draw the text. The function simply creates a square for each char of the text. We create the vertices `Pn` according to the `font_width` and `font_height` (cf. fig2). Then we apply the texture on each rectangle.

To apply the texture, we need to know  the ASCII value of each char, and then pick the right letter from the texture. The code below only works with textures following the same rules as the textures provided in this repository (cf fig1).

```c++
void init_text(text *t){
  float font_height = 1.0f;
  float font_width  = 0.5f;
  int length = t->value.size();           // Number of char to draw
  std::vector<vertex_opengl>  geometrie;  // We need 4 vertices per char
  std::vector<triangle_index> index       // We need 2 triangles per char

  int ascii_offset = 32;                // ASCII code of 1st char in texture file is 32
  int width        = 30;                // 30 char per line in texture file
  float x_tick     = 0.0333f;           // .. so 1/30 char horizontally
  float y_tick     = 0.2f;              // And 1/5 char vertically

  // Here we go, for each char, we create a rectangle.
  for(int i = 0 ; i < length; i++){
    //Vertices coordinates
    vec3 p0=vec3( i*font_width    ,        0.0f, 0.0f);
    vec3 p1=vec3( i*font_width    , font_height, 0.0f);
    vec3 p2=vec3( (i+1)*font_width, font_height, 0.0f);
    vec3 p3=vec3( (i+1)*font_width,        0.0f, 0.0f);

    //Vertices normal
    vec3 n0=vec3(0.0f,0.0f,1.0f);
    vec3 n1=n0;
    vec3 n2=n0;
    vec3 n3=n0;

    //Vertices color
    vec3 c0=vec3(1.0f,1.0f,1.0f);
    vec3 c1=c0;
    vec3 c2=c0;
    vec3 c3=c0;

    //Vertices texture
    int ascii_code      = (int)t->value[i];                    // Current char ASCII value
    int texture_code    = ascii_code - ascii_offset;           // Current char index in our texture
    float texture_x = (texture_code % width) * x_tick;         // Current char horizontal position
    float texture_y = (int)(texture_code / width) * y_tick;    // Current char vertical position
    vec2 t0=vec2(texture_x         , texture_y + y_tick);
    vec2 t1=vec2(texture_x         , texture_y         );
    vec2 t2=vec2(texture_x + x_tick, texture_y         );
    vec2 t3=vec2(texture_x + x_tick, texture_y + y_tick);

    geometrie.push_back(vertex_opengl(p0, n0, c0, t0));
    geometrie.push_back(vertex_opengl(p1, n1, c1, t1));
    geometrie.push_back(vertex_opengl(p2, n2, c2, t2));
    geometrie.push_back(vertex_opengl(p3, n3, c3, t3));

    index.push_back(triangle_index(i*4, i*4+1, i*4+2));
    index.push_back(triangle_index(i*4, i*4+2, i*4+3));
  }

  glGenVertexArrays(1, &t->vao);                                              CHECK_GL_ERROR();
  glBindVertexArray(t->vao);                                                  CHECK_GL_ERROR();

  glGenBuffers(1,&t->vbo);                                                    CHECK_GL_ERROR();
  glBindBuffer(GL_ARRAY_BUFFER,t->vbo);                                       CHECK_GL_ERROR();
  glBufferData(GL_ARRAY_BUFFER,geometrie.size() * sizeof(vertex_opengl), &geometrie[0], GL_STATIC_DRAW);   CHECK_GL_ERROR();

  glEnableVertexAttribArray(0); CHECK_GL_ERROR();
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex_opengl), 0); CHECK_GL_ERROR();

  glEnableVertexAttribArray(1); CHECK_GL_ERROR();
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, sizeof(vertex_opengl), (void*)sizeof(vec3)); CHECK_GL_ERROR();

  glEnableVertexAttribArray(2); CHECK_GL_ERROR();
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(vertex_opengl), (void*)(2*sizeof(vec3))); CHECK_GL_ERROR();

  glEnableVertexAttribArray(3); CHECK_GL_ERROR();
  glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(vertex_opengl), (void*)(3*sizeof(vec3))); CHECK_GL_ERROR();

  glGenBuffers(1,&t->vboi);                                                   CHECK_GL_ERROR();
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,t->vboi);                              CHECK_GL_ERROR();
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,index.size() * sizeof(index),&index[0],GL_STATIC_DRAW);   CHECK_GL_ERROR();

  load_texture("data/fontB.tga",&t->texture_id);
}
```
*Note: We pass a pointer  to our `text` structure so we can use this function on several text structures.*

 


#### Draw function
We only need to pass the `transformation` of our `text` structure as uniforms and load its `vao`, `vbo` and `vboi`.

 


```c++
void draw_texts(text *text_t){
  //Send uniforma parameters
  {
    GLint loc_rotation_model = glGetUniformLocation(shader_program_id, "rotation_model"); CHECK_GL_ERROR();
    if (loc_rotation_model == -1) std::cerr << "Pas de variable uniforme : rotation_model" << std::endl;
    glUniformMatrix4fv(loc_rotation_model,1,false,pointeur(text_t->transform.rotation));    CHECK_GL_ERROR();

    vec3 c = text_t->transform.rotation_center;
    GLint loc_rotation_center_model = glGetUniformLocation(shader_program_id, "rotation_center_model"); CHECK_GL_ERROR();
    if (loc_rotation_center_model == -1) std::cerr << "Pas de variable uniforme : rotation_center_model" << std::endl;
    glUniform4f(loc_rotation_center_model , c.x,c.y,c.z , 0.0f);                                 CHECK_GL_ERROR(); 

    vec3 t = text_t->transform.translation;
    GLint loc_translation_model = glGetUniformLocation(shader_program_id, "translation_model"); CHECK_GL_ERROR();
    if (loc_translation_model == -1) std::cerr << "Pas de variable uniforme : translation_model" << std::endl;
    glUniform4f(loc_translation_model , t.x,t.y,t.z , 0.0f);                                     CHECK_GL_ERROR();
  }

  glBindVertexArray(text_t->vao);CHECK_GL_ERROR();
  {
    glBindTexture(GL_TEXTURE_2D, text_t->texture_id);                       CHECK_GL_ERROR();
    int nbr_triangles = text_t->value.size()*2;    // We draw two triangles per char.
    glDrawElements(GL_TRIANGLES, 3*nbr_triangles, GL_UNSIGNED_INT, 0);               CHECK_GL_ERROR();
  }
}
```